from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


def load_webdriver_f_headless_standalone_chromewd():
    """
    required 4445 wd - available at :webdriver_vault/webdriver_vault/headless_standalone_chromewd_3_14/up.sh
    """
    SELENIUM_HUB = 'http://localhost:4445/wd/hub'
    wd = webdriver.Remote(
        command_executor=SELENIUM_HUB,
        desired_capabilities=DesiredCapabilities.CHROME,
    )
    wd.implicitly_wait(20)
    wd.maximize_window()
    return wd
