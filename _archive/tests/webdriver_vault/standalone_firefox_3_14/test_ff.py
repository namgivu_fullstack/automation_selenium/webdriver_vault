from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class Test:

    def test(self):
        """
        required 4446 wd - available at ./webdriver_vault/standalone-chrome-3.141/up.sh
        """
        SELENIUM_HUB = 'http://localhost:4446/wd/hub'; wd = webdriver.Remote(
            command_executor=SELENIUM_HUB,
            desired_capabilities=DesiredCapabilities.FIREFOX,
        )

        wd.get('http://www.google.com')
        assert wd.title == 'Google'
        wd.quit()
