# intro
Infrastructure that creates webdriver vault aka selenium hub+node used in selenium scripts
ref. https://github.com/namgivu/web-automation-testing/tree/master/webdriver_vault

# prerequisite
Require ubuntu server 18.04
Require docker, docker-compose ref. bit.ly/nndockercompose
```bash
docker --version; docker-compose --version
    should_see='
        Docker version 19.03.11, build dd360c7
        docker-compose version 1.25.5, build unknown
    '
```


# serve wd:4444 seleniumhub:3.11
```bash
: you@git-cloned-home
    ./webdriver_vault/seleniumhub-3.14/up.sh
    : open firewall to allow outbound rule at port 4444
```


# serve wd:4445 standalone-chrome:3.141
```bash
: you@git-cloned-home
    ./webdriver_vault/standalone-chrome-3.14/up.sh
    : open firewall to allow outbound rule at port 4445
```


# serve wd:4446 standalone-firefox:3.141
```bash
: you@git-cloned-home
    ./webdriver_vault/standalone-firefox-3.14/up.sh
    : open firewall to allow outbound rule at port 4446
```


# misc
```bash
dockerclearall
    ./webdriver_vault/seleniumhub-3.14/up.sh ; ./webdriver_vault/standalone-chrome-3.14/up.sh ; ./webdriver_vault/standalone-firefox-3.14/up.sh
