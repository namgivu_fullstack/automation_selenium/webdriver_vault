#!/usr/bin/env bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/.." && pwd)
cd $AH
    PYTHONPATH=$AH  pipenv run  pytest -p no:warnings  --tb=short       -n1
    #               .           .      no warning      traceback short  =run in parallel threads  
