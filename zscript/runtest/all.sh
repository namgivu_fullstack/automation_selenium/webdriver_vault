#!/usr/bin/env bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/.." && pwd)
cd $AH
    PYTHONPATH=$AH  python3 -m pipenv run  pytest -p no:warnings  --tb=short            -n1
    #               .       .         .    !      -p no warning   --tb=traceback short  =run in parallel threads
