from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


WD_IMPLICITLY_WAIT=6


def load_webdriver_f_autoinstall_chrome_wd(headless=False):  # f aka from
    o = webdriver.ChromeOptions()

    #region handle :headless
    if headless:
        '''ref. https://dev.to/googlecloud/using-headless-chrome-with-cloud-run-3fdp'''
        o.add_argument("--headless")
        o.add_argument("--disable-gpu")
        # chrome_options.add_argument("window-size=1024,768")  # we want it maximize set as below eg wd.maximize_window()
        o.add_argument("--no-sandbox")
    #endregion handle :headless

    wd = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=o)
    wd.implicitly_wait(WD_IMPLICITLY_WAIT)
    wd.maximize_window()
    return wd
