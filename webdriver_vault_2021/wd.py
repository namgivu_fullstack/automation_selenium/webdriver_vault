from .choice01_autoinstall_chrome_wd.wd import load_webdriver_f_autoinstall_chrome_wd
from .choice01f_autoinstall_firefox_wd.wd import load_webdriver_f_autoinstall_firefox_wd
from .choice02_downloaded_chrome_wd.wd import load_webdriver_f_downloaded_chrome_wd
from .choice03_headless_standalone_chrome_wd_3_14.wd import load_webdriver_f_headless_standalone_chrome_wd


class WEBDRIVER_CHOICE:
    c01__autoinstall_chrome_wd             = 1
    c01__autoinstall_chrome_wd__headless   = 10
    c01b__autoinstall_firefox_wd           = 4
    c02__downloaded_chrome_wd              = 2
    c03__headless_standalone_chromewd_3_14 = 3


CHOICE_WEBDRIVER_DICT = {
    WEBDRIVER_CHOICE.c01__autoinstall_chrome_wd             : {'load_wd_func': load_webdriver_f_autoinstall_chrome_wd,         'args': {}                },
    WEBDRIVER_CHOICE.c01__autoinstall_chrome_wd__headless   : {'load_wd_func': load_webdriver_f_autoinstall_chrome_wd,         'args': {'headless':True} },
    WEBDRIVER_CHOICE.c01b__autoinstall_firefox_wd           : {'load_wd_func': load_webdriver_f_autoinstall_firefox_wd,        'args': {}                },
    WEBDRIVER_CHOICE.c02__downloaded_chrome_wd              : {'load_wd_func': load_webdriver_f_downloaded_chrome_wd,          'args': {}                },
    WEBDRIVER_CHOICE.c03__headless_standalone_chromewd_3_14 : {'load_wd_func': load_webdriver_f_headless_standalone_chrome_wd, 'args': {}                },
}


def load_webdriver(choice=WEBDRIVER_CHOICE.c01__autoinstall_chrome_wd):
    d            = CHOICE_WEBDRIVER_DICT.get(choice)
    load_wd_func = d['load_wd_func']
    args         = d['args']
    if not load_wd_func: raise Exception(f'webdriver choice not found for key={choice} in dict :CHOICE_WEBDRIVER_DICT')
    else:
        wd=load_wd_func(**args)
        return wd
